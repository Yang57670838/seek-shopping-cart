const { merge } = require('webpack-merge');
const commonConfig = require('./common')
const devServer = require('./devServer');

const devConfig = {
    devtool: 'inline-source-map'
  };

module.exports = (configDirs) => {
    return merge(commonConfig(configDirs), devServer, devConfig);
}