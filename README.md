### `run locally`

yarn install<br />
yarn start

### `run with nginx in production env`

docker build -t yang/shopping-cart:v1 . <br />
docker run --rm -it  --name shopping-cart -p 8080:80 yang/shopping-cart:v1

### `type check`

yarn run type-check

### `unit test`

yarn run test <br />
yarn run test-coverage