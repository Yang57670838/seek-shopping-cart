const path = require('path');

const configDirs = {
    BUILD_DIR: path.resolve(__dirname, './dist'),
    APP_DIR: path.resolve(__dirname, './src'),
    PUBLIC_DIR: path.resolve(__dirname, './public')
}

module.exports = env => {
    if (env.dev) {
        return require('./config/dev.js')(configDirs);
    }
    if (env.prod) {
        return require('./config/prod.js')(configDirs);
    }
}; 