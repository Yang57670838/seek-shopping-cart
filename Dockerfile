FROM node:10-alpine AS builder
WORKDIR /app
COPY package*.json ./
RUN yarn install --production && yarn run build-prod
COPY . ./

FROM nginx:alpine
WORKDIR /usr/share/nginx/html
RUN rm -rf ./*
COPY --from=builder /app/build .
ENTRYPOINT ["nginx", "-g", "daemon off;"]