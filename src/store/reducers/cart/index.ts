import { Reducer } from 'redux'
import { actions as ActionTypes } from 'constants/action'
import { CartStore, CartActions } from 'types/cart'
import addOneProductHandler from './handler/addOneProductHandler'
import removeOneProductHandler from './handler/removeOneProductHandler'

const initState: CartStore = {
    items: null
};

export const cartReducer: Reducer<CartStore, CartActions> = (state: CartStore = initState, action: CartActions): CartStore => {
    switch (action.type) {
        case ActionTypes.ADD_ONE_PRODUCT:
            return addOneProductHandler(state, action.id)
        case ActionTypes.REMOVE_ONE_PRODUCT:
            return removeOneProductHandler(state, action.id)
        // to do: fix, currently all variable (initState/ current state) will becomes previous state.. have to use fixed value
        case ActionTypes.LOGOUT as any:
            return {
                items: null
            }
        default:
            return state;
    }
}