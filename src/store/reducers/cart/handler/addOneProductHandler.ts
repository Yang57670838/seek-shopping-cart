import { CartStore } from 'types/cart';

function addOneProductHandler(state: CartStore, id: number): CartStore {

    if (state.items === null) {
        state.items = {
            [id]: 1
        }
    } else {
        if (id in state.items) {
            state.items[id]++
        } else {
            state.items = {
                ...state.items,
                [id]: 1
            }
        }
    }
    return state
}

export default addOneProductHandler
