import { CartStore } from 'types/cart';

function removeOneProductHandler(state: CartStore, id: number): CartStore {

    if (state.items) {
        if (id in state.items && state.items[id] > 0) {
            state.items[id]--
        } else {
            state.items = {
                ...state.items,
                [id]: 0
            }
        }
    }

    return state
}

export default removeOneProductHandler
