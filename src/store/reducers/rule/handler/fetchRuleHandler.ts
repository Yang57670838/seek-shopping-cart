import { RuleStore } from 'types/rule'
import { discountRules } from 'mocks/rules'

function fetchRuleHandler(state: RuleStore, companyName: string): RuleStore {

    if (companyName in discountRules) {
        return {
            rules: discountRules[companyName],
            error: false,
        }
    }
    return {
        rules: [],
        error: true
    }
}

export default fetchRuleHandler
