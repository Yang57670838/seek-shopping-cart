import { Reducer } from 'redux'
import { actions as ActionTypes } from '../../../constants/action'
import { RuleStore, RuleActions } from '../../../types/rule'
import fetchRuleHandler from './handler/fetchRuleHandler'

const initialState: RuleStore = {
    rules: [],
    error: false,
};

export const ruleReducer: Reducer<RuleStore, RuleActions> = (state: RuleStore = initialState, action: RuleActions): RuleStore => {
    switch (action.type) {
        case ActionTypes.FETCH_CALCULATING_RULES:
            return fetchRuleHandler(state, action.companyName)
        default:
            return state;
    }
}