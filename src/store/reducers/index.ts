import { loginReducer } from './login'
import { productReducer } from './product'
import { cartReducer } from './cart'
import { ruleReducer } from './rule'
import { combineReducers } from 'redux'
import { actions as ActionTypes } from 'constants/action'
import { Store } from 'types/common/store'

const appReducer = combineReducers({
    login: loginReducer,
    product: productReducer,
    cart: cartReducer,
    rule: ruleReducer
})

const rootReducer = (state: Store, action: any) => {
    if (action.type === ActionTypes.LOGOUT) {
        //  persist store will also then save the init state of login reducer, which is empty data..
        return appReducer(undefined, action);
    }
    return appReducer(state, action);
};


export default rootReducer

export type AppState = ReturnType<typeof rootReducer>

