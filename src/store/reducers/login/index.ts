import { Reducer } from 'redux'
import { actions as ActionTypes } from 'constants/action'
import { LoginStore, LoginActions } from 'types/login'
import loginHandler from './handler/loginHandler'

const initialState: LoginStore = {
  companyName: null,
  exp: null,
  token: null,
  iss: null,
};

export const loginReducer: Reducer<LoginStore, LoginActions> = (state: LoginStore = initialState, action: LoginActions): LoginStore => {
  switch (action.type) {
    case ActionTypes.LOGIN:
      return loginHandler(state, action.loginName);
    default:
      return state;
  }
}