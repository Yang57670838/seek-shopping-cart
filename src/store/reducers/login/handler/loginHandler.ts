import jwt_decode from 'jwt-decode'
import { LoginStore, Customer } from 'types/login'
import { JWTmock } from 'mocks/customers'

function loginHandler(state: LoginStore, loginName: Customer): LoginStore {

    const currentTime = Math.round(Date.now() / 1000)
    const decodedPayload: any = jwt_decode(JWTmock.value)

    return {
        companyName: loginName, // mock from login page input instead of fixed mock jwt
        exp: currentTime+3600, // mock to expire after one hour
        token: JWTmock.value,
        iss: decodedPayload.iss
    }
}

export default loginHandler
