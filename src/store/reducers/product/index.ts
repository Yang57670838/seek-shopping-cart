import { Reducer } from 'redux'
import { mapKeys } from 'lodash'
import { actions as ActionTypes } from 'constants/action'
import { ProductStore, ProductActions } from 'types/products'
import { products } from 'mocks/products'

// normalize the array of products into redux store
const normalizedProducts = mapKeys(products, 'productId' )

const initialState: ProductStore = {
    products: null
};

export const productReducer: Reducer<ProductStore, ProductActions> = (state: ProductStore = initialState, action: ProductActions): ProductStore => {
  switch (action.type) {
    case ActionTypes.FETCH_PRODUCTS:
      return {
        ...state,
        products: normalizedProducts
      };
    default:
      return state;
  }
}