import * as loginActions from './login'
import * as productActions from './product'
import * as cartActions from './cart'
import * as ruleActions from './rule'

export { loginActions, productActions, cartActions, ruleActions }
