import { actions as ActionTypes } from 'constants/action'
import { Customer, LoginAction, LogoutAction } from 'types/login'

// since there is no backend, there will be no redux middleware here
export const loginAction: (loginName: Customer) => LoginAction = (loginName) => {
    return {
        type: ActionTypes.LOGIN,
        loginName
    }
}

export const logoutAction: () => LogoutAction = () => {
    return {
        type: ActionTypes.LOGOUT,
    }
}
