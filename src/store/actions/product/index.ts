import { actions as ActionTypes } from 'constants/action'
import { FetchProducts } from 'types/products'

// since there is no backend, there will be no redux middleware here
export const fetchProductsAction: () => FetchProducts = () => {
    return {
        type: ActionTypes.FETCH_PRODUCTS,
    }
}
