import { actions as ActionTypes } from 'constants/action'
import { FetchRule } from 'types/rule'

// since there is no backend, there will be no redux middleware here
export const fetchRuleAction: (companyName: string) => FetchRule = (companyName) => {
    return {
        type: ActionTypes.FETCH_CALCULATING_RULES,
        companyName
    }
}
