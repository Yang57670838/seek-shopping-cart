import { actions as ActionTypes } from 'constants/action'
import { AddOneProduct, RemoveOneProduct } from 'types/cart'

// since there is no backend, there will be no redux middleware here
export const addOneProductAction: (id: number) => AddOneProduct = (id) => {
    return {
        type: ActionTypes.ADD_ONE_PRODUCT,
        id
    }
}

export const removeOneProductAction: (id: number) => RemoveOneProduct = (id) => {
    return {
        type: ActionTypes.REMOVE_ONE_PRODUCT,
        id
    }
}
