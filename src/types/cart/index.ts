import { actions as ActionTypes } from 'constants/action'

export interface AddOneProduct {
    type: ActionTypes.ADD_ONE_PRODUCT;
    id: number;
}

export interface RemoveOneProduct {
    type: ActionTypes.REMOVE_ONE_PRODUCT;
    id: number;
}

export type CartActions =
| AddOneProduct
| RemoveOneProduct;

export interface CartStore {
    items: {
        [productId: number]: number
    } | null
}


