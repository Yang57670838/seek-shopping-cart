import { LoginStore } from '../login'
import { ProductStore } from '../products'
import { CartStore } from '../cart'
import { RuleStore } from '../rule'

export interface Store {
    login: LoginStore;
    product: ProductStore;
    cart: CartStore;
    rule: RuleStore;
}