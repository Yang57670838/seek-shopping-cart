import { actions as ActionTypes } from 'constants/action'

export type Customer = 'SecondBite' | 'Axil Coffee Roasters' | 'MYER' | 'random customer' | ''

export interface LoginAction {
    type: ActionTypes.LOGIN;
    loginName: Customer;
}

export interface LogoutAction {
    type: ActionTypes.LOGOUT;
}

export type LoginActions =
| LoginAction
| LogoutAction;

export interface LoginStore {
    companyName: Customer | null;
    exp: number | null;
    token: string | null;
    iss: string | null;
}


