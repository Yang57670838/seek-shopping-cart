import { actions as ActionTypes } from 'constants/action'

export interface Rule {
    productId: number;
    productNumber: number;
    discount: string;
}

export interface Rules {
    [companyName: string]: Rule[]
}

export interface FetchRule {
    type: ActionTypes.FETCH_CALCULATING_RULES;
    companyName: string; // will fetch a list of rules from mock based on unique company name
}

export type RuleActions =
| FetchRule;

export interface RuleStore {
    rules: Rule[]
    error: boolean;
}