import { actions as ActionTypes } from 'constants/action'

export interface Product {
    name: 'Classic Ad' | 'Stand out Ad' | 'Premium Ad';
    productId: number;
    description: string;
    price: string;
}

export interface FetchProducts {
    type: ActionTypes.FETCH_PRODUCTS;
}

export type ProductActions =
| FetchProducts;

export interface ProductStore {
    products: {
        [productId: number]: Product
    } | null
}


