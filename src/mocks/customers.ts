import { Customer } from 'types/login'

export const customers: Customer[] = [
    'random customer',
    'SecondBite',
    'Axil Coffee Roasters',
    'MYER'
]

export const JWTmock = {
    value: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzZWVrLmNvbSIsImV4cCI6MTYzNTA3OTczNywic3ViIjoiMTIzNDUiLCJhdWQiOiJzZWVrU2hvcHBpbmdDYXJ0IiwianRpIjoiMTExMTEiLCJuYW1lIjoiWWFuZyBMaXUifQ.n02KUbz6pR7JR7APddpegI8u2zV6M1HSo2-f_Cw5dYQ',
    header: {
        'alg': 'HS256',
        'typ': 'JWT'
    },
    payload: {
        'iss': 'seek.com',
        'exp': 1635079737, // 2021-10-24
        'sub': '12345',
        'aud': 'seekShoppingCart',
        'jti': '11111',
        'name': 'MYER'
    }
}
