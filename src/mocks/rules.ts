import { Rules } from 'types/rule'

// mock the dynamic discount rules as object, potentially  database can store company Name (or company unique ID) as index
// each company partition, will have a list of discout rules (object here)
export const discountRules: Rules  = {
    'SecondBite': [
        {
            productId: 1,
            productNumber: 3,
            discount: '269.99' // Gets a 3 for 2 deal on Classic Ads
        }
    ],
    'Axil Coffee Roasters': [
        {
            productId: 2,
            productNumber: 1,
            discount: '23' // 322.99 - 299.99 per Stand out Ad
        }
    ],
    'MYER': [
        {
            productId: 2,
            productNumber: 5,
            discount: '322.99'
        },
        {
            productId: 3,
            productNumber: 1,
            discount: '5' // 394.99 - 389.99 per Premium Ad
        }
    ]
}