import { Product } from 'types/products'

export const products: Product[] = [
    {
        name: 'Classic Ad',
        productId: 1,
        description: 'Offers the most basic level of advertisement',
        price: '269.99'
    },
    {
        name: 'Stand out Ad',
        productId: 2,
        description: 'Allows advertisers to use a company logo and use a longer presentation text',
        price: '322.99'
    },
    {
        name: 'Premium Ad',
        productId: 3,
        description: 'Same benefits as Standout Ad, but also puts the advertisement at the top of this results, allowing higher visibility',
        price: '394.99'
    }
]