import { RouteComponentProps } from 'react-router';
import { UnregisterCallback, Href, Location } from 'history';

// mock out the dependencies for react router
// data is the route query params object
export default function getMockRouterProps<P>(data: P) {
  const location: Location = {
    hash: '',
    key: '',
    pathname: '',
    search: '',
    state: {}
  };

  const props: RouteComponentProps<P> = {
    match: {
      isExact: true,
      params: data,
      path: '',
      url: ''
    },
    location,
    history: {
      length: 2,
      action: 'POP',
      location,
      push: () => {},
      replace: () => {},
      go: num => {},
      goBack: () => {},
      goForward: () => {},
      block: t => {
        const temp: UnregisterCallback = null as any;
        return temp;
      },
      createHref: t => {
        const temp: Href = '';
        return temp;
      },
      listen: t => {
        const temp: UnregisterCallback = null as any;
        return temp;
      }
    },
    staticContext: {}
  };

  return props;
}
