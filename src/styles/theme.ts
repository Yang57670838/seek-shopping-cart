import { createMuiTheme } from '@material-ui/core/styles';
import { IThemeOptions } from 'types/common/theme'

const seekBlue: string = '#133980'
const seekRed: string = '#E61778'

export default createMuiTheme({
    palette: {
        primary: {
          main: `${seekBlue}`,
        },
        secondary: {
          main: `${seekRed}`,
        },
      },
    typography: {
        tab: {
            textTransform: 'none',
            fontweight: 700,
            fontSize: '1rem',
        },
        headerButton: {
            fontSize: '1rem',
            textTransform: 'none',
            color: 'white'
        }
    }
  } as IThemeOptions)