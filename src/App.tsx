import React from 'react';
import { ThemeProvider } from '@material-ui/core/styles'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import theme from './styles/theme'
import Page from 'components/pages/Page'
import Checkout from 'components/pages/Checkout'
import Login from 'components/pages/Login'
import Header from 'components/organisms/Header'
import Logout from 'components/pages/Logout'
import ProtectedRoute from 'components/pages/ProtectedRoute'

const App: React.FC = () => {
  return (
    <ThemeProvider theme={theme}>
      <BrowserRouter>
        <Header />
        <Switch>
          <ProtectedRoute exact={true} path='/' component={Page} />
          <Route exact={true} path='/login' component={Login} />
          <ProtectedRoute exact={true} path='/checkout' component={Checkout} />
          <Route exact={true} path='/logout' component={Logout} />
        </Switch>
      </BrowserRouter>
    </ThemeProvider>
  );
}

export default App;
