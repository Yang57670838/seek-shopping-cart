// a list of products display with flex box
import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import { productActions } from 'store/actions'
import { Store } from 'types/common/store'
import { Product } from 'types/products'
import ProductComponent from '../../molecules/Product'

const useStyles = makeStyles({
    productsWrapper: {
        display: 'inline-flex',
        width: '100%',
        justifyContent: 'space-between',
        flexWrap: 'wrap'
    },
    paper: {
        flexBasis: '30%',
        marginBottom: '30px'
    }
});

interface StateProps {
    availableProducts: { [productId: number]: Product } | null;
}

interface DispatchProps {
    fetchAvailableProducts: () => void;
}

type AllProps = StateProps & DispatchProps;


const Products: React.FC<AllProps> = (props) => {

    const classes = useStyles()

    useEffect(() => {
        props.fetchAvailableProducts()
    }, []) // to do: fix fetch during routing

    return (
        <React.Fragment>
            <div className={classes.productsWrapper}>
                {
                    props.availableProducts && Object.keys(props.availableProducts as object).map(k => {
                        if (props.availableProducts) {
                            return <ProductComponent productDetails={props.availableProducts[Number(k)]} key={k} />
                        }
                        return null
                    })
                }
            </div>
            {
                !props.availableProducts && (
                    <Typography variant='h5'>
                        we dont offer products currently..
                    </Typography>
                )
            }
        </React.Fragment>
    );
}

const mapStateToProps: (state: Store) => StateProps = state => ({
    availableProducts: state.product.products
});

const mapDispatchToProps: DispatchProps = {
    fetchAvailableProducts: productActions.fetchProductsAction
}

export default connect(mapStateToProps, mapDispatchToProps)(Products)