import React, { useState, useEffect } from 'react'
import { Link, withRouter, RouteComponentProps } from 'react-router-dom'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import useScrollTrigger from '@material-ui/core/useScrollTrigger'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import Button from '@material-ui/core/Button'
import { makeStyles, createStyles } from '@material-ui/core/styles'
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer'
import MenuIcon from '@material-ui/icons/Menu'
import Iconbutton from '@material-ui/core/IconButton'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import { ITheme } from 'types/common/theme'

// jss
const styles = (theme: ITheme) => createStyles({
    toolbarMargin: {
        ...theme.mixins.toolbar,
        backgroundColor: 'red'
    },
    tabContainer: {
        marginLeft: 'auto'
    },
    tab: {
        ...theme.typography.tab,
        minWidth: 10,
        marginLeft: '25px'
    },
    appBar: {
        zIndex: theme.zIndex.modal + 1
    },
    button: {
        ...theme.typography.headerButton,
        borderRadius: '50px',
        marginLeft: '25px',
        marginRight: '25px',
        height: '45px'
    },
    drawerIconContainer: {
        marginLeft: 'auto',
        '&:hover': {
            backgroundColor: 'transparent'
        }
    },
    drawerIcon: {
        height: '40px',
        weight: '40px'
    },
    drawerItem: {
        ...theme.typography.tab,
        opacity: 0.7
    },
    drawerItemSelected: {
        ...theme.typography.tab,
        opacity: 1
    },
    headerButton: {
        backgroundColor: '#E61778'
    },
    headerButtonText: {
        color: 'white',
        ...theme.typography.tab,
        opacity: 1
    },
    headerButtonTextSelected: {
        color: 'white',
        ...theme.typography.tab,
        opacity: 0.7
    }
} as any)// to do: ts consider string as invalid type of css property

const useStyles = makeStyles(styles);

function ElevationScroll(props: {children: React.ReactElement}) {
    const { children } = props;

    const trigger = useScrollTrigger({
        disableHysteresis: true,
        threshold: 0
    });

    return React.cloneElement(children, {
        elevation: trigger ? 4 : 0
    });
}

export interface Props extends RouteComponentProps<{}> {}

const Header: React.FC<Props> = (props) => {

    const classes = useStyles()
    const [currentTab, setTab] = useState<number>(0)
    const [openDrawer, setOpenDrawer] = useState<boolean>(false)
    const theme = useTheme()
    const smallScreenMatched = useMediaQuery(theme.breakpoints.down('md'))
    // const iOS = process.browser && /iPad|iPhone|iPod/.test(navigator.userAgent);

    const tabRoutes:{ label: string, path: string }[] = [
        {
            label: 'Home', path: '/',
        },
        {
            label: 'Checkout', path: '/checkout'
        }
    ]

    useEffect(() => {
        if (window.location.pathname === '/' && currentTab !== 0) {
            setTab(0)
        } else if (window.location.pathname === '/checkout' && currentTab !== 1) {
            setTab(1)
        }
    }, [])

    const handleTabChange = (e: React.ChangeEvent<{}>, value: number) => {
        setTab(value)
    }

    const handleLogout = () => {
        props.history.push('/logout')
    }

    const handleCloseDrawer = () => {
        setOpenDrawer(false)
    }

    const handleOpenDrawer = () => {
        setOpenDrawer(true)
    }

    const handleChangeDrawerStatus = (status: boolean) => (e: React.MouseEvent<HTMLElement>)  => {
        setOpenDrawer(status)
    }

    const handleMenuClickEvent = (index: number) => (e: React.MouseEvent<HTMLElement>) => {
        handleCloseDrawer()
        setTab(index);
    }

    const tabs = (
        <React.Fragment>
            <Tabs
                className={classes.tabContainer}
                value={currentTab}
                onChange={handleTabChange}
                indicatorColor='primary'
            >
                {tabRoutes.map((r, i) => {
                    return (
                        <Tab key={`tab_${i}`} className={classes.tab} component={Link} to={r.path} label={r.label} />
                    )
                })}
            </Tabs>
            <Button variant='contained' color='secondary' className={classes.button} onClick={handleLogout}>
                Logout
            </Button>
        </React.Fragment>
    )

    const drawer = (
        <React.Fragment>
            <SwipeableDrawer
                // disableBackdropTransition={!iOS}
                // disableDiscovery={iOS}
                open={openDrawer}
                onClose={handleCloseDrawer}
                onOpen={handleOpenDrawer}
            >
                <div className={classes.toolbarMargin} />
                <List disablePadding>
                    {tabRoutes.map((r, i) => {
                        return (
                            <ListItem
                                onClick={handleMenuClickEvent(i)}
                                divider
                                button
                                component={Link}
                                to={r.path}
                                selected={currentTab === i}
                                key={`list_tab_${i}`}
                            >
                                <ListItemText
                                    disableTypography
                                    className={currentTab === i ? classes.drawerItemSelected : classes.drawerItem}
                                >
                                    {r.label}
                            </ListItemText>
                            </ListItem>
                        )
                    })}
                    <ListItem
                        onClick={handleMenuClickEvent(tabRoutes.length)}
                        divider
                        button
                        component={Link}
                        to='/logout'
                        selected={currentTab === tabRoutes.length}
                        className={classes.headerButton}
                    >
                        <ListItemText
                            disableTypography
                            className={currentTab === tabRoutes.length ? classes.headerButtonText : classes.headerButtonTextSelected}
                        >
                            Logout
                        </ListItemText>
                    </ListItem>
                </List>
            </SwipeableDrawer>
            <Iconbutton className={classes.drawerIconContainer} onClick={handleChangeDrawerStatus(!openDrawer)} disableRipple>
                <MenuIcon color='secondary' className={classes.drawerIcon} />
            </Iconbutton>
        </React.Fragment>
    )
    // because we need to material ui click effect when click tabs, we have to put it inside the react-route
    // so we hide header for pages dont need header component
    // to do: think better solution
    if (window.location.pathname === '/login') return null;

    return (
        <React.Fragment>
            <ElevationScroll>
                <AppBar position='fixed' className={classes.appBar}>
                    <Toolbar>
                        <Typography variant='h3'>
                            Seek
                        </Typography>
                        {
                            smallScreenMatched ? drawer : tabs
                        }
                    </Toolbar>
                </AppBar>
            </ElevationScroll>
            <div className={classes.toolbarMargin} />
        </React.Fragment>
    );
}

export default withRouter(Header)