// one product card
import React, { useState } from 'react'
import { connect } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import { cartActions } from 'store/actions'
import { Product } from 'types/products'

const useStyles = makeStyles({
    root: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        minWidth: 275,
        flexBasis: '30%',
        marginBottom: '30px'
    },
    priceWrapper: {
        marginBottom: 12
    }
});

interface DispatchProps {
    addOne: (id: number) => void;
    removeOne: (id: number) => void;
}

interface OwnProps {
    productDetails: Product;
}

type AllProps = DispatchProps & OwnProps;


const ProductComponent: React.FC<AllProps> = ({ productDetails, addOne, removeOne }) => {

    const classes = useStyles()

    const handleAddOne = () => {
        addOne(productDetails.productId)
    }

    const handleRemoveOne = () => {
        removeOne(productDetails.productId)
    }

    return (
        <React.Fragment>
            <Card className={classes.root}>
                <CardContent>
                    <Typography variant='h5' component='h2'>
                        {productDetails.name}
                    </Typography>
                    <Typography className={classes.priceWrapper} color='textSecondary'>
                        {productDetails.price}
                    </Typography>
                    <Typography variant='body2' component='p'>
                        {productDetails.description}
                    </Typography>
                </CardContent>
                <CardActions>
                    <Button size='small' color='primary' onClick={handleAddOne}>Add 1</Button>
                    <Button size='small' color='secondary' onClick={handleRemoveOne}>Delete 1</Button>
                </CardActions>
            </Card>
        </React.Fragment>
    );
}

const mapDispatchToProps: DispatchProps = {
    addOne: cartActions.addOneProductAction,
    removeOne: cartActions.removeOneProductAction
}

export default connect(undefined, mapDispatchToProps)(ProductComponent)