import React, { useEffect } from 'react'
import Paper from '@material-ui/core/Paper';
import { connect } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import { Store } from 'types/common/store'
import { Product } from 'types/products'
import { ruleActions } from 'store/actions'
import { Rule } from 'types/rule'

const useStyles = makeStyles({
    checkoutWrapper: {
        display: 'flex',
        width: '100%',
        justifyContent: 'space-between',
        flexWrap: 'wrap'
    },
    itemListWrapper: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        flexBasis: '40%',
    },
    item: {
        margin: '25px',
        padding: '25px',
        minWidth: 275,
    },
    pricingWrapper: {
        flexBasis: '40%'
    }
});

interface MergedItem extends Product {
    number: number
}

interface StateProps {
    items: { [productId: number]: number } | null;
    products: { [productId: number]: Product } | null;
    customerName: string | null;
    mergedItems: MergedItem[] | null;
    currentRules: Rule[];
}

interface DispatchProps {
    fetchRules: (companyName: string) => void;
}

type AllProps = StateProps & DispatchProps;

const Checkout: React.FC<AllProps> = (props) => {

    const classes = useStyles()

    useEffect(() => {
        if (props.customerName) {
            props.fetchRules(props.customerName)
        }
    }, [])

    // potentially, the calculating can happen in the server side, to make it more accurate and safe
    let totalItems = ''
    let price: number = 0
    let discount = 0
    let total = 0
    if (props.mergedItems && props.currentRules) {
        for (let i = 0; i < props.mergedItems.length; i++) {
            if (props.mergedItems[i].number > 0) {
                totalItems = `${props.mergedItems[i].name} ${totalItems}`
                const currentPrice = (parseFloat(props.mergedItems[i].price) * props.mergedItems[i].number)
                price = price + currentPrice
                total = total + currentPrice
                // calculate discount
                // to do: normalize store structure for downloaded rules can optimize this logic
                const matchedRuleIndex = props.currentRules.findIndex(r=> props.mergedItems && r.productId === props.mergedItems[i].productId)
                if (matchedRuleIndex >= 0) {
                    const priceCutTarget = props.currentRules[matchedRuleIndex].productNumber
                    let priceCut = parseFloat(props.currentRules[matchedRuleIndex].discount)
                    // calculate how many discounts for this product can have
                    const cutTimes = Math.floor(props.mergedItems[i].number / priceCutTarget)
                    priceCut = priceCut * cutTimes
                    discount = discount + priceCut
                    total = total - priceCut
                }
            }
        }
    }

    return (
        <div className={classes.checkoutWrapper}>
            <div className={classes.itemListWrapper}>
                {
                    props.mergedItems && props.mergedItems.map((item, i) => {
                        return (
                            <Paper elevation={3} className={classes.item} key={i}>
                                <Typography variant='body2' component='p'>
                                    { item && item.name } <br />
                                    Bought: { item && item.number }
                                </Typography>
                            </Paper>
                        )
                    })
                }
                {
                    !props.mergedItems && (
                        <Paper elevation={3} className={classes.item}>
                            <Typography variant='body2' component='p'>
                                You havn't bought anything yet.
                            </Typography>
                        </Paper>
                    )
                }
            </div>
            <div className={classes.pricingWrapper}>
                <Paper elevation={3} className={classes.item}>
                    <Typography variant='body2' component='p'>
                        Customer: {props.customerName} <br />
                        Items: {totalItems} <br />
                        Price: +${price.toFixed(2)} <br />
                        Discount: -${discount.toFixed(2)}<br />
                        Total: ${total.toFixed(2)}
                    </Typography>
                </Paper>
            </div>
        </div>
    );
}

const mapStateToProps: (state: Store) => StateProps = state => ({
    items: state.cart.items,
    products: state.product.products,
    customerName: state.login.companyName,
    currentRules: state.rule.rules,
    mergedItems: state.cart.items && Object.keys(state.cart.items).map(id => {
        if (state.product.products) {
            return {
                ...state.product.products[Number(id)],
                number: state.cart.items && state.cart.items[Number(id)]
            }
        }
        return null
    }) as MergedItem[]
});

const mapDispatchToProps: DispatchProps = {
    fetchRules: ruleActions.fetchRuleAction
}

export default connect(mapStateToProps, mapDispatchToProps)(Checkout)