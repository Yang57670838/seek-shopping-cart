import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { RouteComponentProps } from 'react-router'
import { loginActions } from 'store/actions'

interface DispatchProps {
    logout: () => void;
}

interface OwnProps extends RouteComponentProps { }

export type AllProps = DispatchProps & OwnProps;


export const Logout: React.FC<AllProps> = (props) => {

    useEffect(() => {
        props.logout()
        props.history.push('/login')
    }, [])

    return (
        <React.Fragment>
            ...loging out now
        </React.Fragment>
    );
}

const mapDispatchToProps: DispatchProps = {
    logout: loginActions.logoutAction
}

export default connect(undefined, mapDispatchToProps)(Logout)