import React from 'react'
import Typography from '@material-ui/core/Typography'
import Products from '../../organisms/Products'

const Page: React.FC = () => {
    return (
        <>
            <Typography variant='h2'>
                We Offer
            </Typography>
            <Products />
        </>
    );
}

export default Page;