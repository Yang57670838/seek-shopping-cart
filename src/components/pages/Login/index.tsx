import React, { useState, useEffect } from 'react'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import InputLabel from '@material-ui/core/InputLabel'
import FormControl from '@material-ui/core/FormControl'
import Button from '@material-ui/core/Button'
import Paper from '@material-ui/core/Paper'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import { connect } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'
import { RouteComponentProps } from 'react-router'
import { customers } from 'mocks/customers'
import { loginActions } from 'store/actions'
import { Customer } from 'types/login'
import { Store } from 'types/common/store'

interface StateProps {
    isLogin: boolean;
    companyName: boolean;
}

interface DispatchProps {
    login: (loginName: Customer) => void;
}

interface OwnProps extends RouteComponentProps {}

type AllProps = StateProps & DispatchProps & OwnProps;

const useStyles = makeStyles({
    loginCard: {
        marginTop: '30vh'
    },
    paper: {
        padding: '50px'
    },
    button: {
        width: '100%'
    },
    formControl: {
        minWidth: '200px'
    }
})

const Login: React.FC<AllProps> = (props) => {

    const [loginName, setLoginName] = useState<Customer>('')

    const classes = useStyles()

    useEffect(() => {
        if (props.companyName) {
          props.history.push('/')
        }
      }, [props.isLogin])

    const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        setLoginName(event.target.value as Customer)
    }

    const handleLogin = () => {
        props.login(loginName)
    }

    return (
        <div className={classes.loginCard}>
            <Grid container spacing={0} justify='center' direction='row'>
                <Grid item>
                    <Paper
                        variant='elevation'
                        elevation={2}
                        className={classes.paper}
                    >
                        <Grid
                            container
                            direction='column'
                            justify='center'
                            spacing={2}
                        >
                            <Grid item>
                                <Typography variant='h5'>
                                    Seek
                                </Typography>
                            </Grid>
                            <Grid item>
                                <FormControl className={classes.formControl}>
                                    <InputLabel id='login-company-select-label'>Company Name</InputLabel>
                                    {/* mock privileged customers or random customers into select options for login */}
                                    <Select
                                        labelId='login-company-select-label'
                                        id='login-company-select'
                                        value={loginName}
                                        onChange={handleChange}
                                    >
                                        {
                                            customers.map((c, i) => (
                                                <MenuItem key={i} value={c}>{c}</MenuItem>
                                            ))
                                        }
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item>
                                <Button
                                    variant='contained'
                                    color='primary'
                                    type='submit'
                                    className={classes.button}
                                    onClick={handleLogin}
                                    disabled={!loginName}
                                >
                                    Login
                                </Button>
                            </Grid>
                        </Grid>
                    </Paper>
                </Grid>
            </Grid>
        </div>
    );
}

const mapStateToProps: (state: Store) => StateProps = state => ({
    isLogin: state.login && !!state.login.companyName, // check if login state is with data or not
    companyName: state.login && !!state.login.companyName
});

const mapDispatchToProps: DispatchProps = {
    login: loginActions.loginAction
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)