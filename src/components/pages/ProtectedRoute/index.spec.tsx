import { expired } from './index';

describe('<ProtectedRoute />', () => {

    const mockEpochLater = Date.now() / 1000 + 1
    const mockEpochNow = Date.now() / 1000
    const mockEpochBefore = Date.now() / 1000 - 1

    it('expired function is working correctly', () => {
        expect(expired(mockEpochLater)).toBeFalsy()
        expect(expired(mockEpochNow)).toBeTruthy()
        expect(expired(mockEpochBefore)).toBeTruthy()
    });
});