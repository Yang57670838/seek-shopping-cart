import React from 'react'
import { connect } from 'react-redux'
import { RouteProps } from 'react-router'
import { LoginStore } from 'types/login'
import { Store } from 'types/common/store'
import { Route } from 'react-router-dom'
import Logout from '../Logout'

export const expired = (epoch: number): boolean => {
    const currentEpoch = Date.now() / 1000
    return currentEpoch>epoch
}

interface StoreProps {
    loginInfo?: LoginStore;
}

export interface ComponentProps extends StoreProps, RouteProps { }

export const ProtectedRoute: React.FC<ComponentProps> = (props: ComponentProps) => {
    const { component: Component, loginInfo, ...rest } = props;

    // mock to check if companyName is cached in redux, however, in real case, may need to check not only token, but also expire time
    const isAccountValid = loginInfo && loginInfo.exp && loginInfo.token && !expired(loginInfo.exp)
    return <Route {...rest} component={isAccountValid ? Component : Logout} />;
};

const mapStateToProps = (state: Store) => ({
    loginInfo: state.login
});

export default connect(
    mapStateToProps,
    null,
)(ProtectedRoute);
